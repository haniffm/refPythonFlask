# pull official base image
FROM python:3.8.5-slim-buster

# set work directory
WORKDIR /usr/src/app

# set environment variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

# Install system dependencies
RUN apt-get update && apt-get install -y netcat

# install dependencies
RUN pip install --upgrade pip
COPY Pipfile* ./
RUN pip install pipenv && pipenv install --system --deploy

# copy project
COPY ./src/ ./

# run entrypoint.sh
ENTRYPOINT ["/usr/src/app/entrypoint.sh"]
