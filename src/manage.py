from flask.cli import FlaskGroup

from project import app, db, User


cli = FlaskGroup(app)



@cli.command("create_db")
def create_db():
    print("recreating db...")
    db.drop_all()
    db.create_all()
    db.session.commit()
    print("db recreated!")


@cli.command("seed_db")
def seed_db():
    db.session.add(User(email="haniffm@kth.se"))
    db.session.commit()


if __name__ == "__main__":
    cli()

