# RefPythonFlask
---------------
A reference Python Flask app.

## Quick start

Prerequisits:
- Python 3.8
- Pip3
- Docker
- docker-compose


Create an .env file:
```
cp .env.example .env
```

### Run

In Docker:
```
docker-compose up --build -d
```

Re create DB:
```
docker-compose exec web python manage.py create_db
```

Seed DB:
```
docker-compose exec web python manage.py seed_db
```

In local dev:
```
pip install pipenv && pipenv install && pipenv run python src/manage.py run
```
